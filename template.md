---
reduce-color: true
titlepage: false
document-type: "Beschluss <nummer>"
institution: "<datum_unterschrift>"
event-type: "<titel>"
date: ""
lang: "de"
title: "Beschluss <nummer>: <titel>"
...

# <titel>

<gremium> hat in seiner **<sitzung>** vom **<datum_sitzung>** gemäß **<grundlage>** folgenden Beschluss gefasst:  
  
<beschluss>  
  
Der Beschluss wurde angenommen mit folgendem Stimmverhältnis (bei insgesamt **<sitze>** Sitzen):  
  
<stimmverhältnis>  

---

Göttingen, den <datum_unterschrift>  

![](sign_<sign>.png){ width=181px }

Für das Präsidium, der Präsident

\vspace*{\fill}

<gremium>  
Präsidium • <präsidium>  
<mail> • <website>  
<adresse> • <telefon>  
