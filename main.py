#!/usr/bin/env python3
__version__ = "0.0.1a1"

import argparse
#import time
import json

# todo try except blocks around file io lines

def load_config(path):
    """
    load config file
    and ask for missing data
    """
    with open(path, 'r') as f:
        config = json.load(f)
    return config

def load_resolution(path):
    """
    load resolution text containing title in first line
    """
    with open(path, 'r') as f:
        lines = f.readlines()
    title = lines[0][2:-1]  # first line without '# ' and '\n'
    # todo correct trim title
    resolution = "".join(lines[2:])   # todo test newlines
    return resolution, title

def load_meta(path):
    """
    load general meta file for all generated resolutions
    and ask for missing data
    """
    with open(path, 'r') as f:
        meta = json.load(f)
    return meta

def load_basis(path="grundlagen.json"):
    with open(path, 'r') as f:
        basis = json.load(f)
    return basis

def load_template(path="template.md"):
    with open(path, 'r') as f:
        template = f.read()
    return template

def build_resolution(template, resolution, substitutions):
    """
    template: template string
    resolution: string with markdown formatted resolution text
    substitutions: list of dicts that keys are replaced with its values in the template
    """
    content = template
    for sub, value in substitutions.items():
        content = content.replace(f"<{sub}>", str(value))
    content = content.replace("<beschluss>", resolution)
    output = f"{substitutions['nummer']}_{substitutions['titel']}.md"

    with open(output, 'w+') as f:
        f.write(content)
    pandoc_command = "" # todo generate pandoc command

    # dirty quick lazy fix needed for local shell script
    with open(output[:-3]+"_meta.md", 'w+') as f:
        f.write("")

    return pandoc_command

def load_missing(text):
    # todo import from terminal.py
    value = input(text)
    return value

def select_basis(basis, title):
    selection = ""
    print(f"Beschlussgrundlagenauswahl Stand {basis['version']}")
    for idx, val in enumerate(basis['OrgS'].keys()):
        print(f"{idx}: \tOrgS {val}")
    for idx, val in enumerate(basis['FinO'].keys()):
        print(f"f{idx}: \tFinO {val}")
    print(f"Auswahl treffen für Beschluss {title}")
    while not selection:
        selected = input("Beschlussgrundlage auswählen (Mehrfachauswahl mit Komma trennen): ")
        texts = []
        for s in selected.split(','):
            # todo trim s
            # todo try except
            if s.startswith('f'):
                law = "FinO"
                article = int(s[1:])
            else:
                law ="OrgS"
                article = int(s)
            vals = list(basis[law].values())
            _ = vals[article]
            texts.append(f"{_} {law}")
        selection = f" {basis['delimiter']} ".join(texts)  # todo test selection string
    return selection

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('resolutions', nargs='+', help='markdown formatted resolution files')
    parser.add_argument('-m', '--meta')
    parser.add_argument('-c', '--config')
    parser.add_argument('-t', '--template')
    parser.add_argument('-g', '--gui', action='store_true')
    parser.add_argument('-v', '--version', action='version', version=f'%(prog)s {__version__}')
    args = parser.parse_args()

    if args.gui:
        print("no gui implemeted yet. sorry.")

    resolution_paths    = args.resolutions
    meta_path           = args.meta         if args.meta        else None   # ask missing interactive
    config_path         = args.config       if args.config      else None   # ask missing interactive
    template_path       = args.template     if args.template    else "template.md"
    # time.strftime('%Y-%m-%d')

    config  = load_config(  path = config_path )
    # load missing config
    meta    = load_meta(    path = meta_path )
    # load missing general meta
    template = load_template(path=template_path)
    basis   = load_basis()  # todo add to args

    substitutions = config | meta

    for r in resolution_paths:
        resolution, title = load_resolution(path=r)
        substitutions['titel'] = title
        substitutions['nummer'] = r.split('/')[-1][:8] # read beschluss_nummer from filename
        # load missing specific meta
        # * ask grundlage
        substitutions['grundlage'] = select_basis(basis=basis, title=title)
        print(f"Building resolution {title}")
        pandoc_command = build_resolution(template=template, resolution=resolution, substitutions=substitutions)
        # call pandoc
        print("Done. you need to call pandoc by yourself")

    # beschluss vars
    # * grundlage -> interaktiv abfragen
    # * stimmverhältnis -> aus ronils csv2md
    """
    if args.gui:
        import gui
        interface = gui.Gui(meeting)
    else:
        import terminal
        interface = terminal.Terminal(meeting)
    interface.run()
    """

if __name__ == "__main__":
    main()
