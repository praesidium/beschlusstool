# Beschlusstool

Dieses Tool kann Beschlüsse für das Studierendenparlament Göttingen generieren.
Zur Nutzung sind die hier ausgeführten Schritte notwendig.

## Installation

### Requirements

* python version 3.9+
* [pandoc](https://pandoc.org/) (optional, for pdf output)
  * [pandoc-Template](https://gitlab.gwdg.de/praesidium/protokolltemplate) gemäß [eigener Anleitung](https://gitlab.gwdg.de/praesidium/protokolltemplate#usage) einrichten
* tk (optional, for gui usage)
* basic knowledge about [json](https://de.wikipedia.org/wiki/Json) and [markdown](https://de.wikipedia.org/wiki/Markdown)

### Setup

1. Dieses Repo klonen, z.B. mit `git clone git@gitlab.gwdg.de:praesidium/beschlusstool.git`
2. `config/example.json` kopieren und anpassen
3. Optionale Dinge wie pandoc einrichten (siehe [Requirements](#requirements))

## Usage

To use the script prepare your resolution, config and meta files and then use main.py as described below.
This project has some example files to help you write your own files.

```
main.py [-h] [-m META] [-c CONFIG] [-t TEMPLATE] [-g] [-v] resolutions [resolutions ...]
```

Example using the example files:

```
./main.py -m resolutions/example.json -c config/example.json resolutions/example.md
```

After starting the script, missing config data and the [Grundlagen](#grundlagen) will be asked.
Follow the printed instructions.

### h

print help and exit

### m

Path to json-file containing meta data about all given resolutions, e.g. date or name. See `resolutions/example.json` for an example.

### c

Path to json-file containing configuration data about all given resolutions, e.g. date. See resolutions/example.json for an example.

### t

Path to md-file which is the used template to generated the finished resolutions in md-format. Per default `template.md` is used.

### g

Use gui instead of terminal. Not gui implemented yet.

### v

print version and exit

### resolutions

Paths to files containing md-format resolution texts.
First line of each resolution must be the resolution title as h1.
See `resolutions/example.md` for an example.

## Grundlagen

Die Grundlagen befinden sich in der Datei `grundlagen.json` und wurden im Sommer 2021 aus den geltenden Ordnungen herausgearbeitet.
Sollten sich die Ordnungen an gewissen Stellen ändern, muss auch die Datei grundlagen.json entsprechend angepasst werden.
Dieses Projekt soll immer die Aktuell geltenden Grundlagen gemäß den Ordnungen in der Datei `grundlagen.json` beinhalten und muss daher ab und zu geupdated werden.

## Templates

There might be confusion about two different types of templates in this project.
First template is the md-template, which is used to generate the resolution md-files.
Second template type is the LaTeX-template, which is used by pandoc to generate the resolution pdf-files when given the resolution md-files.

This section is about the md-templates.

The template contains parts like `<example>`.
These parts are subsituted by the main.py script from this repository.

* *todo*: list all template variables and explain them

## Known issues

* no whitespace allowed in *grundlagen* input
* no gui yet
* no pandoc call yet
* delimiter `i.V.m.` is always used when multiple grundlagen are existend
* parts of the project are in german while other parts are in english
* missing checks if an optional parameter is given and if according file exists
* no input sanitation yet. error will occur if resolution title contains `/` or other forbidden chars

## Lizenz

Die Dateien in diesem Projekt sind unter GNU AFFERO GENERAL PUBLIC LICENSE (AGPL) lizenziert.
Mehr Details stehen in der [LICENSE](LICENSE).
